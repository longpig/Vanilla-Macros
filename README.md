# Vanilla 1.12 Macro Project

> Inspiration and guidance for macro making.

---

## How To Use

Either click on the folders and text files on the github page to find more info or 
click the green code button and select Download ZIP to download the files.
[Notepad](https://notepad-plus-plus.org/) is useful to read or edit the files.

---

## References

- [Complete list of every addon or macro function](http://wowwiki.wikia.com/index.php?title=World_of_Warcraft_API&oldid=281620)
- [LUA tips, chat & raid macros](https://nirklars.wordpress.com/wow/vanilla-wow-lua-tips/)
- [Useful macros (1.0)](https://wowwiki-archive.fandom.com/wiki/Useful_macros_(1.0))
- [Vanilla wow macros](https://nirklars.wordpress.com/wow/vanilla-wow-macros/)
- [Most Used Macros](https://web.archive.org/web/20060813140631/http://www.wowwiki.com/Most_Used_Macros)
- [Macros For Dummies](https://www.ownedcore.com/forums/world-of-warcraft/world-of-warcraft-guides/1038-macros-dummies.html)
- [Useful rogue macro](http://roguecrap.blogspot.com/2006/01/useful-rogue-macro-updated-030206.html)
- [Macro per maghi](https://www.freeforumzone.com/discussione.aspx?idd=5581207)
- [Priest addons and Macros](http://orderofsargeras.com/phpBB2_resto/viewtopic.php?t=14&sid=d4903119e59b7a30276b0df4e64b3aed)
- [Graguk's Warlock Macros](http://blue.cardplace.com/cache/wow-warlock/905421.htm)
- [Lua Wiki](https://vanilla-wow-archive.fandom.com/wiki/Lua)

---

## Useful Addon
- [WowLuaVanilla](https://github.com/laytya/WowLuaVanilla)

---
